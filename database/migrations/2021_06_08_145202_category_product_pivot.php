<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CategoryProductPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_product_pivot', function(Blueprint $table){
            $table->id();
            $table->bigInteger('category_id');
            $table->bigInteger('product_id');
            $table->timestamps();
        });

        // Schema::table('category_product_pivot', function(Blueprint $table){
        //     $table->foreign('category_id')
        //         ->on('categories')
        //         ->references('id');
        //         // ->onDelete('cascade');

        //     $table->foreign('product_id')
        //         ->on('products')
        //         ->references('id');
        //         // ->onUpdate('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_product_pivot');
    }
}
