<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width"/>
        <link rel="stylesheet" href="{{ asset('css/foundation-emails.css') }}">
        <style type="text/css">
            * {
                font-family: Verdana;
            }
            table.items-widget {
                border-collapse: collapse;
                border: #ccc solid 1px;
                width: 100%;
                margin: 1em 0;
            }
            table.items-widget  td {
                text-align: center;
            }

            table.items-widget thead th {
                background-color: #e6e6e6;
            }
        </style>
    </head>
<body>
    <p>
        Nazwa: {{ $name }}<br/>
        Termin realizacji: {{ $timeLine }}
    </p>

    @if ($generateVat)
        <p style="color:red;">Zamawiający prosi o wystawienie faktury VAT</p>
    @endif

    <table class="row">
        <tr>
            <td class="columns small-12 medium-6" >
                <p><strong>Zamawiający</strong></p>
                <address>
                    {{ $customer["firstName"] }}&nbsp;{{ $customer["surName"] }}<br/>
                    {{ $customer["city"] }}&nbsp;ul.{{ $customer["street"] }}&nbsp;{{ $customer["homeNumber"] }}<br/>
                    {{ $customer["postCode"] }}&nbsp;{{ $customer["postOffice"] }}<br/>
                    E-Mail: <a href="mailto:{{ $customer["email"] }}">{{ $customer["email"] }}</a><br/>
                    Tel.: {{ $customer["phone"] }}
                </address>
            </td>
            <td class="expander"></td>
            <td class="columns small-12 medium-6" style="vertical-align:top;">
                <p><strong>Adres dostawy</strong></p>
                <address>
                    {{ $delivery["city"] }}&nbsp;ul.{{ $delivery["street"] }}&nbsp;{{ $delivery["homeNumber"] }}<br/>
                    {{ $delivery["postCode"] }}&nbsp;{{ $delivery["postOffice"] }}<br/>
                </address>
            </td>
        </tr>
    </table>

    @if ($safeLength["enable"])
        <span style="color:red;margin-top:1em;">W elementach uwzględniono zapas długości ({{ $safeLength["value"]*100 }}&nbsp;cm)</span>
    @endif
    <table class="items-widget">
        <thead>
            <tr>
                <th>Szerokość<br/>[cm]</th>
                <th>Wysokość<br/>[cm]</th>
                <th>Długość<br/>[m]</th>
                <th>Ilość<br/>[szt]</th>
                <th>Kubatura<br/>[m<sup>3</sup>]</th>
                <th>Wartość<br>[PLN]</th>
            </tr>
        </thead>
        <tbody>
            @foreach($items as $item)
            @php
                if ($safeLength["enable"])
                    $item["length"] += $safeLength["value"];
            @endphp
                <tr>
                    <td>{{ $item["width"] }}</td>
                    <td>{{ $item["height"] }}</td>
                    <td>{{ $item["length"] }}</td>
                    <td>{{ $item["qty"] }}</td>
                    <td>{{ $item["stere"] }}</td>
                    <td>{{ $item["price"] }}</td>
                </tr>
            @endforeach

            <!-- Wiersz podsumuwujący wszystkie elementy -->
            <tr style="font-weight:bold;background-color:#e6e6e6;">
                <td colspan="4" style="text-align:right;">RAZEM</td>
                <td>{{ $totalStere }}&nbsp;m<sup>3</sup></td>
                <td>{{ $totalPrice }}&nbsp;PLN</td>
            </tr>
        </tbody>
    </table>

    @if (!empty($comment))
        <h4>Komentarz</h4>
        <pre>{!! $comment !!}</pre>
    @endif
</body>
</html>
