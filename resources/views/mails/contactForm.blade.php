<div>
    <address style="border-left:orange solid 3px; border-top-left-radius: 3px; border-bottom-left-radius: 3px; padding-left: 6px;">
        {{ $firstName }}&nbsp;{{ $surName }}<br/>
        Mail: {{ $userMail }}<br/>
        Tel.: {{ $phone }}
    </address>

    <span style="padding-top: 24px;">{{ $content }}</span>
</div>
