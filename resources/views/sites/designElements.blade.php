@extends('layouts.site.page')

@section('title', 'Drewniane elementy konstrukcyjne oraz wyroby z drewna')

@section('meta')
    <meta name="description" content="Sprawdź cenę swojego zamówienia nie wychodząc z domu!" />
@endsection

@section('content')
    <article class="grid-container">
        <h1>Drewnianie elementy konstrukcyjne oraz wyroby z drewna</h1>
        <p>
            Zajmujemy się produkcją elementów konstrukcyjnych dla budownictwa.
            Realizyjemy zamówienia na więźby dachowe, domy o konstrukcji drewnianej (Kanadyjki)
            oraz inne projekty.
        </p>

        <section class="callout success">
            <h2 class="text-center">Sprawdź wycenę zamówienia nie wychodząc z domu</h2>
            <p>
                Kożystając z naszego kreatora wycen możesz samodzelnie sprawdzić
                koszt produkcji twojej konstrukcji. Zamówienie nie jest realizowane od razu,
                do realizacji przystępujemy po wcześniejszym dokładnym ustaleniu
                ceny oraz wpłaceniu 10% zaliczki.
            </p>

            <div class="text-center" style="margin:0; padding:0;">
                <a href="{{ route('site.order.creator') }}" class="button large primary">Przejdź do kreatora</a>
            </div>
        </section>
    </article>
@endsection
