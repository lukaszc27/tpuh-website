@extends('layouts.site.page')

@section('title', 'O Nas - '.setting('site.title'))

@section('meta')
    <meta name="description" content="" />
@endsection

@section('content')
    <article class="grid-container">
        <h1 data-aos="fade-right">O nas</h1>
        <section data-aos="zoom-in">
        <p>
            Istniejemy na rynku od 2005r. w branży budowlanej.
            Oferujemy szeroki asortyment skierowany zarówno do firm budowlanych,
            jak i odbiorców indywidualnych.
        </p>

        <p>
            Wychodząc naprzeciw potrzebom naszych klientów rozszerzamy asortyment o inne produkty
            związane z konstrukcją dachów. Na dzień dzisiejszy jesteśmy dystrybutorem wszelkiego
            rodzaju materiałów budowlanych zwiazanych z budową dachów, tj.okien i schodów
            dachowych FAKRO, membran i foli budowlanych, wszelkiego rodzaju akcesorii jak taśmy,
            wkręty ciesielskie, farmerski i tarasowe. W swojej ofercie posiadamy także szeroki
            wybór chemii budowlanej, gwoździ do gwoździarek, impregnaty do drewna, farby, oleje
            do mebli i tarasów, itp.
        </p>

        <p>Mamy nadzieję, że niskie ceny jak i bogata oferta sprawią, że bedą Państwo zadowoleni.</p>

        <p>
            Współpracujemy z najlepszymi producentami branży budowlanej, prestiżowymi markami, 
            dzięki czemy dopasują Państwo produkt do własnych potrzeb.
        </p>

        <p>
            Nasz towar wyróżnia się jakością, by spełnić oczekiwania odbiorców. 
            Dbając o klienta sukcesywnie staramy się cały czas poszerzać swoją ofertę, 
            by dorównać ciągłym nowinką na rynku.
        </p>

        <h3> Możliwość skompletowania zamówienia:</h3>
        <ul>
            <li>Za pośrednictwem <a target="blank" href="https://allegro.pl/uzytkownik/e-ciesla?bmatch=cl-e2101-d3794-c3683-uni-1-4-0528">sklepu internetowego allegro</a></li>
            <li>telefonicznie: 506 540 125</li>
            <li>drogą e-mail: <a href="mailto:tartakciesla@gmail.com">tartakciesla@gmail.com</a></li>
            <li>odebranie zamówienia na miejscu: 38-305 Lipinki 465 (prosimy o wcześniejszy kontakt telefoniczny)</li>
        </ul>
        </section>
    </article>
@endsection

@section('styles')
    <style type="text/css">
        p, li, a { font-size: 1.1em; }
    </style>
@endsection
