@extends('layouts.site.master')

@section('title', setting('site.title').' - Zapraszamy!')

@section('meta')
    <meta name="description" content="{{ setting('site.description') }}" />
@endsection

@section('content')
    <article class="grid-container home">
        <h2 data-aos="zoom-out">Czym się zajmujemy?</h2>

        <section class="grid-x">
            <div class="cell small-12 medium-4 offer-card" data-aos="fade-right">
                <img src="{{ asset('img/images/elements.jpg') }}" alt="Elementy konstrukcyjne" />
                <a href="{{ route('site.designElements') }}">
                    <p>Drewniane elementy konstrukcyjne oraz wyroby z drewna</p>
                </a>
            </div>

            <div class="cell small-12 medium-4 offer-card" data-aos="zoom-in">
                <img src="{{ asset('img/images/products.jpg') }}" alt="Artykuły do montarzu dachów" />
                <a href="{{ route('site.catalog') }}">
                    <p>Akcesoria do drewna</p>
                </a>
            </div>

            <div class="cell small-12 medium-4 offer-card" data-aos="fade-left">
                <img src="{{ asset('img/images/services.jpg') }}" alt="Usługi" />
                <a href="">
                    <p>Obróbka drewna dostarczonego przez klienta</p>
                </a>
            </div>
        </section>

        <h2 data-aos="zoom-out">Poznaj nasze produkty</h2>
        {{ menu('categories', 'widgets.site.categories') }}

        <h2 data-aos="zoom-out">Popularne produkty</h2>
        <section class="grid-x grid-margin-x grid-margin-y" data-aos="fade-up">
            @component('widgets.site.product')
                @slot('image', asset('img/products/protektor-olej-do-drewna.jpg'))
                @slot('name', 'PROTEKTOR Olej do drewna')
                @slot('route', route('site.index'))
            @endcomponent

            @component('widgets.site.product')
                @slot('image', asset('img/products/protektor-olej-do-drewna.jpg'))
                @slot('name', 'PROTEKTOR Olej do drewna')
                @slot('route', route('site.index'))
            @endcomponent

            @component('widgets.site.product')
                @slot('image', asset('img/products/protektor-olej-do-drewna.jpg'))
                @slot('name', 'PROTEKTOR Olej do drewna')
                @slot('route', route('site.index'))
            @endcomponent

            @component('widgets.site.product')
                @slot('image', asset('img/products/protektor-olej-do-drewna.jpg'))
                @slot('name', 'PROTEKTOR Olej do drewna')
                @slot('route', route('site.index'))
            @endcomponent
        </section>
    </article>
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/home.css') }}" />
@endsection

@section('scripts')
    <script>
        let downArrow = document.querySelector('a#scrollFromHero');
        downArrow.addEventListener('click', function() {
            let mainDocument = document.querySelector('main');
            mainDocument.scrollIntoView();
        });
    </script>
@endsection
