@extends('layouts.site.catalog')

@section('title', 'Sprawdź wszystkie produkty - '.setting('site.title'))

@section('meta')
    <meta name="description" content="" />
@endsection

@section('content')
    <section class="grid-x grid-margin-x">
        @foreach ($products as $product)
            @component('widgets.site.product')
                @slot('image', asset('storage/'.$product->image))
                @slot('name', $product->name)
                @slot('route', route('site.product', ['id' => $product->id]))
            @endcomponent
        @endforeach
    </section>
@endsection
