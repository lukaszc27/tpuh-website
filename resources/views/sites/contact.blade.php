@extends('layouts.site.page')

@section('title', setting('site.title').' - Kontakt')

@section('meta')
    <meta name="description" content="Odwiedź nasz sklep stacionarny w Lipinkach i skorzysta" />
@endsection

@section('content')
    <article class="grid-container">
        <section class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6">
                <h1>Kontakt</h1>

                <address>
                    <strong>Tartak Produkcyjno-Usługowo-Handlowy "Cieśla"</strong><br/>
                    Sławomir Cieśla<br />
                    38-305 Lipinki 465<br />
                </address>

                <p>
                    Tel: 501 250 383<br/>
                    Tel: 506 540 125<br/>
                    E-Mail: <a href="mailto:tartakciesla@gmail.com">tartakciesla@gmail.com</a>
                </p>

                <h3>Godziny otwarcia</h3>
                <div class="grid-y" style="margin: 1em 0;">
                    <div class="grid-x">
                        <span class="cell small-2">Pon-Pt:</span>
                        <span class="cell auto">8<sup>00</sup> - 20<sup>00</sup></span>
                    </div>
                    <div class="grid-x">
                        <span class="cell small-2">Sob:</span>
                        <span class="cell auto">8<sup>00</sup> - 17<sup>00</sup></span>
                    </div>
                    <div class="grid-x">
                        <span class="cell small-2">Nd:</span>
                        <span class="cell auto">Nieczynne</span>
                    </div>
                </div>

                <p class="callout" style="background-color: #E6CA70">
                    Prowadzimy również sprzedaż wysyłkową,
                    <strong>Zamawiając artykuły bezpośrednio u nas (kontakt poprzez <a href="mailto:tartakciesla@gmail.com">email</a> bądź telefonicznie)
                    zyskasz 15% rabatu</strong>
                </p>

                <h3>Formularz kontaktowy</h3>
                <form-contact></form-contact>
            </div>

            <div class="cell small-12 medium-6" data-aos="fade-left">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1195.880834267503!2d21.299097554529997!3d49.66866985657731!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473dc84f23a2badd%3A0xc13d21d67549b4fe!2sTartak%20produkcyjno-us%C5%82ugowo-handlowy%20%22Cie%C5%9Bla%22!5e1!3m2!1spl!2spl!4v1622127698649!5m2!1spl!2spl" style="border:0; width: 100%; height: 100%" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </section>
    </article>
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/contact.css') }}" />
    <style type="text/css">
        span { font-size: 1.1em; }
    </style>
@endsection
