@extends('layouts.site.page')

@section('title', $product->name.' - '.setting('site.title'))

@section('meta')
    <meta name="description" content="" />
@endsection

@section('content')
    <article class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell small-12 medium-4">
                <img src="{{ asset('storage/'.$product->image) }}"
                    alt="{{ $product->name }}"
                    class="img-thumbnail img-responsive" />
            </div>

            <div class="cell small-12 medium-8">
                <h1>{{ $product->name }}</h1>
                <p>Producent: {{ $dealer->name }}</p>
                <p>Jednostka miary: {!! $unit->full_name !!}</p>
            </div>
        </div>

        <section class="product-description">
            <h2>Opis</h2>
            {!! $product->description !!}
        </section>

        @if (isSet($siblingProducts))
            <h2>Podobne produkty</h2>
            <section class="grid-x grid-margin-x">
                @foreach ($siblingProducts as $sibling)
                    @component('widgets.site.product')
                        @slot('route', route('site.product', ['id' => $sibling->id]))
                        @slot('name', $sibling->name)
                        @slot('image', asset('storage/'.$sibling->image))
                    @endcomponent
                @endforeach
            </section>
        @endif
    </article>
@endsection
