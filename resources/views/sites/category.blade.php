@extends('layouts.site.catalog')

@section('title', $category->title.' - '.setting('site.title'))

@section('meta')
    <meta name="description" content="@if (isSet($category->seo_description)) {{ $category->seo_description }} @else {{ $category->top_description }} @endif" />
@endsection

@section('content')
    {{-- <article class="grid-container"> --}}
        <section class="grid-x grid-padding-x">
            @if (isSet($category->brand_logo) && !is_null($category->brand_logo))
                <div class="cell small-12 medium-4" data-aos="fade-right">
                    <img src="{{  asset('storage/'.$category->brand_logo)  }}"
                        alt="{{ $category->title }}"
                        class="thumbnail" />
                </div>
            @endif

            <div class="cell auto" data-aos="zoom-in-up">
                @if (isSet($category->title))
                    <h1>{{ $category->title }}</h1>
                @endif

                @if (isSet($category->top_description))
                    <div>{!! $category->top_description !!}</div>
                @endif
            </div>
        </section>

        @if (isSet($menuItems) && !is_null($menuItems))
            <ul class="categories align-center">
                @foreach ($menuItems as $item)
                    <li>
                        <a href="{{ route('site.category', ['id' => $item->id]) }}"
                            target="{{ $item->target }}">{{ $item->title }}</a>
                    </li>
                @endforeach
            </ul>
        @endif

        @forelse ($products->chunk(4) as $row)
            <div class="grid-x grid-margin-x grid-margin-y" style="margin: 2em 0">
                @foreach($row as $product)
                    @component('widgets.site.product')
                        @slot('name', $product->name)
                        @slot('image', asset('storage/'.$product->image))
                        @slot('route')
                            @empty($product->url)
                                {{ route('site.product', ['id' => $product->id]) }}
                            @else
                                {{ $product->url }}
                            @endif
                        @endslot
                    @endcomponent
                @endforeach
            </div>
        @empty
            <div style="margin: 2em 0; text-align: center;">
                <h2>Brak produktów w wybranej kategorii</h2>
                <h3>
                    Prosimy o cierpliwość, strona w budowie.
                </h3>
            </div>
        @endforelse

        @if ($category->bottom_description)
            <div>{!! $category->bottom_description !!}</div>
        @endif
    {{-- </article> --}}
@endsection
