<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Łukasz Cieśla" />
    @yield('meta')

    <title>@yield('title')</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/catalog.css') }}" />
    @yield('styles')
</head>
    <body>
        <div id="app">
            @include('widgets.site.navigation')

            <main>
                <article class="container">
                    <div class="grid-x">
                        <div class="cell medium-3">
                            <section class="menu">
                            <h5>Kategorie</h5>
                            <ul>
                                @foreach ($categories as $category)
                                    <li>
                                        <a href="{{ route('site.category', ['id'=>$category['id']]) }}">
                                            {{ $category['title'] }}
                                        </a>

                                        @if (!is_null($category['subcategories']) &&
                                            count($category['subcategories']) > 0)
                                            <img src="{{ asset('img/arrow-right.png')}}" 
                                                alt="Right arrow"
                                                class="arrow" />

                                            <ul>
                                                @foreach ($category['subcategories'] as $sub)
                                                    <li>
                                                        <a href="{{ route('site.category', ['id' => $sub->id]) }}">{{ $sub->title }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                            </section>
                        </div>
                        <div class="cell medium-auto">
                            @yield('content')
                        </div>
                    </div>
                </article>
            </main>

            {{-- @include('widgets.site.footer') --}}
        </div>

        <script src="{{ asset('js/app.js') }}"></script>
        @yield('scripts')
    </body>
</html>
