@extends('layouts.site.page')

@section('title', 'Kreator zamówienia on-line')

@section('meta')
    <meta name="description" content="" />
@endsection


@section('content')
    <article class="grid-container">
        <h1>Kreator zamówienia on-line</h1>

        <div class="grid-x">
            <div class="cell small-12">
                <order-creator></order-creator>
            </div>
        </div>
    </article>
@endsection
