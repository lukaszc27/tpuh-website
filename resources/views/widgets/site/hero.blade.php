<header style="background-image: url('{{ asset('img/hero.png') }}')">
    @include('widgets.site.navigation')

    <section>
        <h1>Tartak Produkcyjno-Usługowo-Handlowy "Cieśla"</h1>
        <a href="{{ route('site.catalog') }}" class="cta-button">Zobacz katalog</a>
    </section>

    <a id="scrollFromHero" class="arrow-button">
        <img src="{{ asset('img/arrow-down.svg') }}" alt="Przejdź dalej" />
    </a>
</header>
