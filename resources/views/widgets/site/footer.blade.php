<footer>
    <div class="grid-x grid-margin-y">
        <div class="cell medium-4 small-12">
            <address>
                <strong>
                    <span>Tartak Produkcyjno-Usługowo-Handlowy "Cieśla"</span>
                </strong>
                <span>Sławomir Cieśla</span>
                <span>
                    <img src="{{ asset('img/home.png') }}" alt="Adres" />
                    <span>38-305 Lipinki 465</span>
                </span>
                <span>
                    <img src="{{ asset('img/phone.png') }}" alt="Telefon" />
                    <span>Tel: 501 250 383</span>
                </span>
                <span>
                    <img src="{{ asset('img/phone.png') }}" alt="Telefon" />
                    <span>Tel: 506 540 125</span>
                </span>
                <span>
                    <img src="{{ asset('img/email.png') }}" alt="Email" />
                    <span>E-Mail: <a href="mailto:tartakciesla@gmail.com">tartakciesla@gmail.com</a></span>
                </span>
            </address>
        </div>

        <div class="cell medium-2 small-12">
            <h6>Przydatne linki</h6>
            <ul>
                <li><a href="{{ route('site.privacy.policy') }}">Polityka prywatności</a></li>
                <li><a href="{{ route('site.privacy.rodo') }}">RODO</a></li>
            </ul>
        </div>

        <div class="cell medium-6 small-12">
            <h6>Formularz kontaktowy</h6>
            <footer-contact></footer-contact>
        </div>
    </div>
</footer>
