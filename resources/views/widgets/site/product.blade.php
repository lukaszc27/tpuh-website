<a href="{{ $route }}" class="cell small-6 medium-3 product-card">
    <img src="{{ $image }}" alt="{{ $name }}" />
    <span>{{ $name }}</span>
</a>
