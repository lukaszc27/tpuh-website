<nav>
    <div class="wrapper">
        <button class="button-toggle" id="toggle">
            <img src="{{ asset('img/toggle.svg') }}" alt="Toggle" />
        </button>

        <div class="nav-brand">
            <a href="{{ route('site.index') }}">
                <img src="{{ asset('img/logo.png') }}" alt="Tartak Produkcyjno-Usługowo-Handlowy Cieśla" />
            </a>
        </div>

        <div class="collapse" id="collapse">
            <a href="{{ route('site.index') }}">
                <img class="collapse-logo" src="{{ asset('img/logo.png') }}" alt="Tartak PUH Cieśla" />
            </a>

            <ul class="top-nav">
                <li>
                    <a href="{{ route('site.catalog') }}" class="menu-link">KATALOG</a>
                </li>
                <li>
                    <a href="{{ route('site.aboutUs') }}" class="menu-link">O NAS</a>
                </li>
                <li>
                    <a href="{{ route('site.contact') }}" class="menu-link">KONTAKT</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

