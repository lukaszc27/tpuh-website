<section class="grid-x grid-margin-x grid-margin-y categories">
    @foreach ($items->chunk(2) as $categories)
        <div class="cell small-12 medium-6">
            <div class="grid-y grid-margin-y" data-aos="zoom-in-down">
                @foreach($categories as $item)
                    <div class="cell category">
                        <a href="{{ route('site.category', ['id' => $item->id]) }}" target="{{ $item->target }}">
                            <p>{{ $item->title }}</p>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    @endforeach
</section>
