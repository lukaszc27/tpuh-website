/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

import $ from 'jquery';
window.$ = window.jQuery = $;

/**
 * uruchomienie biblioteki animacji elementów podczas przewijania strony
 */
import AOS from 'aos';
AOS.init();

import Foundation from 'foundation-sites'
window.Foundation = Foundation;

$(document).foundation();

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('FooterContact', require('./components/site/FooterContact.vue').default);
Vue.component('FormContact', require('./components/site/FormContact.vue').default);
Vue.component('PrivacyPolicy', require('./components/site/PrivacyPolicy.vue').default);
Vue.component('OrderCreator', require('./components/site/orderCreator/Creator.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

/**
 * Całość elementów DOM została wczytana
 * uruchamianie globalnych sktyptów
 */
window.addEventListener('DOMContentLoaded', (event) => {
    let toggleButton = document.getElementById('toggle');
    toggleButton.addEventListener('click', () => {
        let collapseContainer = document.getElementById('collapse');
        collapseContainer.classList.toggle('toggle');
    })
});
