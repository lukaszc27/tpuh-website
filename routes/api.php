<?php

use App\Http\Controllers\OrderCreator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Site;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/contact', [Site::class, 'sendContactRequest'])
    ->name('api.site.contact');

Route::post('/order/create', [OrderCreator::class, 'createOrder'])
    ->name('api.order.create');
