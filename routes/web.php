<?php

use App\Http\Controllers\OrderCreator;
use App\Http\Controllers\Site;
use Illuminate\Support\Facades\Route;
// use TCG\Voyager\Voyager;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Site::class, 'index'])->name('site.index');
Route::get('/kontakt', [Site::class, 'contact'])->name('site.contact');
Route::get('/katalog', [Site::class, 'catalog'])->name('site.catalog');
Route::get('/o-nas', [Site::class, 'aboutUs'])->name('site.aboutUs');
Route::get('/rodo', [Site::class, 'rodoPage'])->name('site.privacy.rodo');
Route::get('/polityka-prywatnosci', [Site::class, 'privacyPolicyPage'])->name('site.privacy.policy');
Route::get('/kategoria/{id}', [Site::class, 'category'])->where('id', '[0-9]+')->name('site.category');
Route::get('/produkt/{id}', [Site::class, 'product'])->where('id', '[0-9]+')->name('site.product');
Route::get('/elementy-konstrukcyjne', [Site::class, 'designElements'])->name('site.designElements');

Route::prefix('order')->group(function () {
    Route::get('/creator', [OrderCreator::class, 'index'])->name('site.order.creator');
});

// Route::group(['prefix' => 'admin'], function () {
//     Voyager::routes();
// });
