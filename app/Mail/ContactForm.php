<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

    public $firstName;
    public $surName;
    public $userMail;
    public $phone;
    public $content;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($firstName, $surName, $mail, $phone, $content)
    {
        $this->firstName = $firstName;
        $this->surName = $surName;
        $this->userMail = $mail;
        $this->phone = $phone;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('ciesla@e-ciesla.pl')
                    ->subject('Wiadomość kontaktkowa ze strony e-ciesla.pl')
                    ->view('mails.contactForm');
    }
}
