<?php

namespace App\Mail;

use DOMDocument;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProductionOrder extends Mailable
{
    use Queueable, SerializesModels;

    public $name;           // nazwa zamówienia
    public $comment;        // komentarz do zamówienia
    public $timeLine;       // termin realizacji
    public $generateVat;    // czy zamawiający chce odczymać fakturę VAT
    public $safeLength;     // długość zapasu dla pojedynczego elementu
    public $items;          // elementy konstrukcyjne
    public $customer;       // osoba zamawiająca
    public $delivery;       // adres dostawy
    public $totalStere;     // całkowita ilość m3 wszystkich elementów
    public $totalPrice;     // całkowita kwota do zapłaty za wszystkie elementy

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($orderName,
                                $timeLine,
                                $customer,
                                $delivery,
                                $items,
                                $safeLength,
                                $generateVat,
                                $totalStere,
                                $totalPrice,
                                $comment = '')
    {
        $this->name = $orderName;
        $this->comment = $comment;
        $this->timeLine = $timeLine;
        $this->generateVat = $generateVat;
        $this->items = $items;
        $this->customer = $customer;
        $this->delivery = $delivery;
        $this->safeLength = $safeLength;
        $this->$totalStere = $totalStere;
        $this->$totalPrice = $totalPrice;

        // var_dump($items);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $xmlDocument = new DOMDocument('1.0', 'UTF-8');
        $root = $xmlDocument->createElement('eSawmill');
        $xmlDocument->appendChild($root);

        // zapis listy elementów
        $orderNode = $xmlDocument->createElement('order');
        foreach ($this->items as $item) {
            // jeśli użytkownik dodał zapas długości dla elementów
            // to w tym miejscu zostaje on dodany do eksportu zamówienia
            if ($this->safeLength["enable"])
                $item["length"] += $this->safeLength["value"];

            $node = $xmlDocument->createElement('item');
            $node->setAttribute('planned', '0');
            $node->setAttribute('quantity', $item["qty"]);
            $node->setAttribute('width', $item["width"]);
            $node->setAttribute('height', $item["height"]);
            $node->setAttribute('length', $item["length"]);
            $orderNode->appendChild($node);
        }
        $root->appendChild($orderNode);

        // zapis nazwy zamówienia (tytułu) oraz komentarza
        $orderName = $xmlDocument->createElement('name');
        $orderName->textContent = $this->name;
        $root->appendChild($orderName);

        $orderDescription = $xmlDocument->createElement('description');

        $description = $this->comment."\r\n";
        if ($this->generateVat)
            $description .= "+ Klient prosi o wystawienie faktury VAT\r\n";

        if (!empty($delivery["city"])) {
            // inforamcja o adresie dostawy
            $description .= "+ Adres dostawy:\r\n";
            $description .= $this->delivery["city"]
                        .' ul.'.$this->delivery["street"]
                        .' '.$this->delivery["homeNumber"]."\r\n";
            $description .= $this->delivery["postCode"].' '.$this->delivery["postOffice"]."\r\n";
        }
        $orderDescription->textContent = $description;
        $root->appendChild($orderDescription);

        // zapis inforamcji na temat deadline dla zamówienia
        $deadline = $xmlDocument->createElement('endDate');
        $deadline->textContent = $this->timeLine;
        $root->appendChild($deadline);

        // eksport informacji o zamawiającym
        $customer = $xmlDocument->createElement('contractor');
        $firstName = $xmlDocument->createElement('firstName');
        $firstName->textContent = $this->customer["firstName"];

        $surName = $xmlDocument->createElement('surName');
        $surName->textContent = $this->customer["surName"];

        $email = $xmlDocument->createElement('email');
        $email->textContent = $this->customer["email"];

        $phone = $xmlDocument->createElement('phone');
        $phone->textContent = $this->customer["phone"];

        $customer->appendChild($firstName);
        $customer->appendChild($surName);
        $customer->appendChild($email);
        $customer->appendChild($phone);

        // eksport adresu osoby zamawiającej
        $address = $xmlDocument->createElement('address');
        $city = $xmlDocument->createElement('city');
        $city->textContent = $this->customer["city"];

        $street = $xmlDocument->createElement('street');
        $street->textContent = $this->customer["street"];

        $homeNumber = $xmlDocument->createElement('homeNumber');
        $homeNumber->textContent = $this->customer["homeNumber"];

        $postCode = $xmlDocument->createElement('postCode');
        $postCode->textContent = $this->customer["postCode"];

        $postOffice = $xmlDocument->createElement('postOffice');
        $postOffice->textContent = $this->customer["postOffice"];

        $address->appendChild($city);
        $address->appendChild($street);
        $address->appendChild($homeNumber);
        $address->appendChild($postCode);
        $address->appendChild($postOffice);

        $customer->appendChild($address);
        $root->appendChild($customer);

        return $this->from('ciesla@e-ciesla.pl')
                    ->subject('Nowe zamówienie - Elementy konstrukcyjne')
                    ->view('mails.productOrder')
                    ->attachData($xmlDocument->saveXML(), 'elementy.xml', [
                        'mime' => 'text/xml'
                    ]);
    }
}
