<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ProductionOrder;
use Illuminate\Support\Facades\Mail;

class OrderCreator extends Controller
{
    public function index()
    {
        return view('creator.designElements');
    }

    public function createOrder(Request $request)
    {
        $ret = Mail::to('lukaszciesla52@gmail.com')->send(new ProductionOrder(
            $request->input('name'),
            $request->input('timeLine'),
            $request->input('customer'),
            $request->input('delivery'),
            $request->input('items'),
            $request->input('safeLength'),
            $request->input('generateVat'),
            $request->input('totalStere'),
            $request->input('totalPrice'),
            $request->input('comment')
        ));

        if (empty($ret)) {
            return [
                'status' => 200
            ];
        }
        else return $ret;
    }
}
