<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Mail\ContactForm;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;
use Illuminate\Support\Facades\Mail;

use function PHPUnit\Framework\isEmpty;

class Site extends Controller
{
    /**
     * Uruchamia główną stronę
     * którą użytkownik widzi zaraz po wybraniu odpowiedniego adresu
     */
    public function index()
    {
        return view('sites.home');
    }

    /**
     * Przezntuję stronę kontakt
     */
    public function contact()
    {
        return view('sites.contact');
    }

    /**
     * wysyła maila na podstawie danych utworzonych z formularza kontaktowego
     */
    public function sendContactRequest(Request $request)
    {
        $validated = $request->validate([
            'mail' => ['email', 'required'],
            'acceptRodo' => ['boolean', 'required'],
            'content' => ['string', 'required'],
            'firstName' => ['string', 'max:64'],
            'surName' => ['string', 'max:64'],
            'phone' => ['string', 'max:12']
        ]);

        $ret = Mail::to('lukaszciesla52@gmail.com')->send(new ContactForm(
            $validated['firstName'],
            $validated['surName'],
            $validated['mail'],
            $validated['phone'],
            $validated['content']
        ));

        if (empty($ret)) {
            return [
                'status' => 200
            ];
        }
        else return $ret;
    }

    /**
     * zwraca stronę informacyjną "O nas"
     */
    function aboutUs()
    {
        return view('sites.aboutUs');
    }

    /**
     * zwraca stronę informującą o RODO
     */
    public function rodoPage()
    {
        return view('sites.policy.rodo');
    }

    /**
     * prezentuje stronę informująca o polityce prywatności
     */
    public function privacyPolicyPage()
    {
        return view('sites.policy.privacyPolicy');
    }

    /**
     * prezentuję stronę informacyjną o elementach konstrukcyjnych
     */
    public function designElements()
    {
        return view('sites.designElements');
    }

    /**
     * Przedstawia informacje o danej kategorii
     * oraz wszystkie podkategorie
     * wyświetla również wszystkie produkty przypisane
     * poszczególnych podkategoriach
     */
    public function category($id)
    {
        $menu = Menu::where('name', 'like', 'categories')->first();
        $categories = MenuItem::where('menu_id', '=', $menu->id)
                            ->where('parent_id', '=', null)->get();

        $menu = collect();
        foreach ($categories as $category) {
            $menu->add([
                'id' => $category->id,
                'title' => $category->title,
                'target' => $category->target,
                'url' => $category->url,
                'subcategories' => MenuItem::where('parent_id', '=', $category->id)->get()
            ]);
        }

        $category = Category::where('menu_item_id', $id)->first();
        $menuItems = MenuItem::where('parent_id', $category->menu_item_id)->get();

        return view('sites.category', [
            'category' => $category,
            'menuItems' => $menuItems,
            'products' => $category->products,
            'categories' => $menu
        ]);
    }

    /**
     * pobiera produkt z bazy danych
     * oraz przedstawia informacje o nim uzytkownikowi
     */
    public function product($id) {
        // return Product::findOrFail($id);
        $product = Product::findOrFail($id);
        $dealer = $product->dealer;
        $siblingProducts = $product->siblingProducts;

        return view('sites.product', [
            'product' => $product,
            'dealer' => $dealer,
            'unit' => $product->unit,
            'siblingProducts' => $siblingProducts
        ]);
    }

    /**
     * zwraca wszystkie produkty zapisane w bazie oraz
     * kategorie produktów utworzone przez użytkownika
     */
    public function catalog()
    {
        $products = Product::all();
        $menu = Menu::where('name', 'like', 'categories')->first();
        $categories = MenuItem::where('menu_id', '=', $menu->id)
                            ->where('parent_id', '=', null)->get();

        $menu = collect();
        foreach ($categories as $category) {
            $menu->add([
                'id' => $category->id,
                'title' => $category->title,
                'target' => $category->target,
                'url' => $category->url,
                'subcategories' => MenuItem::where('parent_id', '=', $category->id)->get()
            ]);
        }

        return view('sites.catalog', [
            'categories' => $menu,
            'products' => $products
        ]);
    }
}
