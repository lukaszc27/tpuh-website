<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models\MenuItem;

class Category extends Model
{
    use HasFactory;

    public function menuItemId() {
        return $this->belongsTo(MenuItem::class, 'menu_item_id', 'id');
    }

    /**
     * relacja wiele do wielu
     * przedstawiająca wszystkie produkty zapisane do danej kategorii
     */
    public function products() {
        return $this->belongsToMany(Product::class, 'category_product_pivot');
    }
}
