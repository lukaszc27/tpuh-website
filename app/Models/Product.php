<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'image', 'name', 'description', 'is_popular',
        'price', 'url'
    ];

    public function dealer() {
        // return $this->hasOne(Dealer::class);
        return $this->belongsTo(Dealer::class);
    }

    public function siblingProducts() {
        return $this->belongsToMany(Product::class,
            'product_product',
            'product_id',
            'parent_id',
            'id');
    }

    public function unit() {
        return $this->belongsTo(Unit::class);
    }
}
